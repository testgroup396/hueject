# gatling-sandbox

## TL;DR
```shell script
git clone https://gitlab.com/tinkoffperfworkshop/part-1/gatling-sandbox.git
cd gatling-sandbox
docker-compose up -d
export PROJECT_REGISTRATION_TOKEN=${PASTE_YOUR_TOKEN_FROM_GITLAB_SETTINGS}
docker run --rm -v gitlab-runner-config:/etc/gitlab-runner --network=gatling-sandbox gitlab/gitlab-runner:v13.4.1 register \
   --non-interactive --url "https://gitlab.com/" --registration-token "${PROJECT_REGISTRATION_TOKEN}" \
   --executor "docker" --docker-image alpine:latest --description "docker-runner" --tag-list "docker,aws" \
   --run-untagged="true" --locked="false" --access-level="not_protected"  --docker-network-mode="gatling-sandbox"
```

## Содержание

* [Общая информация](#общая-информация)
* [Установка](#установка)
* [Настройка](#настройка)
  * [Настроить portainer](#настроить-portainer)
  * [Настроить grafana](#настроить-grafana)
  * [Подключить gitlab агент](#подключить-gitlab-агент)
* [Troubleshooting](#troubleshooting)

## Общая информация

Репозиторий содержит набор контейнеров для ознакомления с примером организации инфрастуктуры вокруг gatling.
docker-compose поднимает контейнеры:
* [influxdb](https://www.influxdata.com/products/influxdb-overview/) - timeseries база данных, используется для хранения клиентских метрик gatling
* [loki](https://grafana.com/oss/loki/) - система аггрегации логов, используется для хранения клиентских логов gatling
* [vector](https://vector.dev/) - коллектор логов, используется для отправки клиентских логов gatling в loki
* [cadvisor](https://github.com/google/cadvisor) - коллектор метрик docker, используется для сбора метрик всего окружения
* [prometheus](https://prometheus.io/) - система мониторинга, используется для сбора и хранения метрик cadvisor
* [grafana](https://grafana.com/) - система визуализации, используется для визуализации метрик/логов
* [gitlab-runner](https://docs.gitlab.com/runner/) - агент gitlab, используется для выполнения gitlab-ci пайплайнов
* [portainer](https://www.portainer.io/) - система менеджмента контейнеров

Общая схема использования:
![gatling-sandbox](img/sandbox-scheme.jpg "gatling-sandbox scheme")

## Установка

<details>
<summary>Как установить docker</summary>

[Общая инструкции по установке docker](https://docs.docker.com/get-docker/)

 **Для пользователей Windows**:
  * Либо [установить WSL 2.0](https://docs.microsoft.com/en-us/windows/wsl/install-win10) и далее следовать инструкции из п.2
  * Либо [установить docker для Windows](https://docs.docker.com/docker-for-windows/install/)

 **Для пользователей Linux/WSL 2.0**:
  * [Установить docker](https://docs.docker.com/engine/install/ubuntu/)
  * [Выполнять команды docker без sudo](https://docs.docker.com/engine/install/linux-postinstall/)
  * [Установить docker-compose](https://docs.docker.com/compose/install/)

 **Для пользователей Mac**:
  * Следовать [инструкции]((https://docs.docker.com/docker-for-mac/install/))

</details>


* Клонировать репозиторий
```shell script
git clone https://gitlab.com/tinkoffperfworkshop/part-1/gatling-sandbox.git
```

* Перейти в директорию с проектом
```shell script
cd gatling-sandbox
```

* Поднять контейнеры с окружением
```shell script
docker-compose up -d
```

<details>
<summary>Пример успешного запуска</summary>

```shell script
$ docker-compose up -d
Creating network "gatling-sandbox" with the default driver
Creating volume "influx-data" with default driver
Creating volume "grafana-data" with default driver
Creating volume "portainer-data" with default driver
Creating volume "gitlab-runner-config" with default driver
Creating influxdb      ... done
Creating portainer     ... done
Creating gitlab-runner ... done
Creating cadvisor      ... done
Creating loki          ... done
Creating grafana       ... done
Creating vector        ... done
Creating prometheus    ... done
```

</details>

* Проверить что все контейнеры поднялись:
```shell script
docker-compose ps -a
```

<details>
<summary>Пример успешного запуска</summary>

```shell script
$ docker-compose ps -a
    Name                   Command                  State                           Ports
--------------------------------------------------------------------------------------------------------------
cadvisor        /usr/bin/cadvisor -logtostderr   Up (healthy)   0.0.0.0:8080->8080/tcp
gitlab-runner   /usr/bin/dumb-init /entryp ...   Up
grafana         /run.sh                          Up             0.0.0.0:3000->3000/tcp
influxdb        /entrypoint.sh influxd           Up             0.0.0.0:2003->2003/tcp, 0.0.0.0:8086->8086/tcp
loki            /usr/bin/loki -config.file ...   Up             0.0.0.0:3100->3100/tcp
portainer       /portainer -H unix:///var/ ...   Up             0.0.0.0:8000->8000/tcp, 0.0.0.0:9000->9000/tcp
prometheus      /bin/prometheus --config.f ...   Up             0.0.0.0:9090->9090/tcp
vector          /usr/local/bin/vector            Up             
```

</details>

**Примечания**:
* Используются следующие порты, необходимо их освободить, либо изменить в файле docker-compose.yml:
`2003, 3000, 3100, 8000, 8080, 8086, 9000, 9090`
* docker-compose создает сеть gatling-sandbox, к ней можно подключать другие контейнеры, указав сеть при запуске:
```shell script
docker run ... --network=gatling-sandbox ...
```
* Для работы рекомендуется иметь свободными 4Гб RAM и 2CPU

## Настройка
### Настроить portainer

* В браузере зайти на страницу [http://localhost:9000](http://localhost:9000)
* В предложенные поля ввести: username: admin, password: любой пароль длинной от 8 символов

### Настроить grafana

* В браузере зайти на страницу [http://localhost:3000](http://localhost:3000)
* Ввести логин/пароль установленный по-умолчанию: admin/admin
* Grafana предложит установить новый пароль, ввести любой (можно повторить пароль admin)

### Подключить gitlab агент

**Предварительно**:
* В браузере открыть https://gitlab.com/
* Зарегистрироваться следуя инструкции или войти под своей учетной записью
* Развернуть окружение gatling-sandbox (gitlab агент разворачивается в составе песочницы)

**Подключить агент**:
[Официальная документация](https://docs.gitlab.com/runner/register/)

* Зайти в настройки агентов проекта (кнопка Settings слева внизу): Settings -> CI/CD -> Runners - Expand
  * В настройках агентов выключить Shared runners, для этого нажать кнопку "Disable shared runners"
  * В настройках агентов найти раздел "Set up a specific Runner manually", в нем скопировать токен из п.3 
    "Use the following registration token during setup"
* Подставить значение токена вместо PROJECT_REGISTRATION_TOKEN и выполнить в консоли команду
```shell script
  docker run --rm -v gitlab-runner-config:/etc/gitlab-runner --network=gatling-sandbox gitlab/gitlab-runner:v13.4.1 register \
   --non-interactive --url "https://gitlab.com/" --registration-token "${PROJECT_REGISTRATION_TOKEN}" \
   --executor "docker" --docker-image alpine:latest --description "docker-runner" --tag-list "docker,aws" \
   --run-untagged="true" --locked="false" --access-level="not_protected"  --docker-network-mode="gatling-sandbox"
  ```
* В настройках агентов проекта Settings -> CI/CD -> Runners - Expand в разделе Runners activated for this project
 должен появится зарегистрированный агент, в состоянии Runner is online
 
**Примечания**:
* gitlab-runner разворачивается в сети gatling-sandbox, он должен быть доступен для систем мониторинга и сбора логов
* При запуске gitlab-ci пайплайна создаются отдельные контейнеры с именем runner(.*), для их создания в сети gatling-sandbox
в команду регистрации добавлен флаг --docker-network-mode="gatling-sandbox". [Подробнее тут](https://docs.gitlab.com/runner/executors/docker.html)
* Регистрация выполняется из другого контейнера, использующего общий том gitlab-runner-config с конфигами gitlab-runner,
после изменения конфигурация применяется автоматически. При возникновении трудностей с выполнением команды регистрации
можно запустить ее внутри контейнера gitlab-runner:
```shell script
docker exec -it gitlab-runner bash

gitlab-runner register \
                 --non-interactive --url "https://gitlab.com/" --registration-token "${PROJECT_REGISTRATION_TOKEN}" \
                 --executor "docker" --docker-image alpine:latest --description "docker-runner" --tag-list "docker,aws" \
                 --run-untagged="true" --locked="false" --access-level="not_protected"  --docker-network-mode="gatling-sandbox"
```

## Troubleshooting
#### Ошибка загрузки конфига
Ошибка иногда возникает на Linux

<details>
<summary>Пример лога с ошибкой запуска prometheus</summary>

```shell script
caller=main.go:290 msg="Error loading config (--config.file=/etc/prometheus/prometheus.yml)" err="open /etc/prometheus/prometheus.yml: permission denied" 
```

</details>

Как исправить:
```shell script
find config/ -type f -exec chmod 644 {} \;
```

#### Ошибка отправки логов в Loki
При долгом старте Loki иногда возникает ошибка: Vector не может отправить сообщения в Loki.

Как обнаружить:

```shell script
docker logs vector
```

Как исправить:

```shell script
docker container restart vector
```

